import cv2
import numpy as np
import time


# read image, turn it gray, return new image
def readImage(imageName):
    image = cv2.imread(imageName, 1)
    image = rgb2gray(image)
    image = cv2.equalizeHist(image)

    return image


# bgr to gray 0.21 R + 0.72 G + 0.07 B
# return new gray image
def rgb2gray(image):
    h, w, channels = image.shape
    newimg = np.zeros([h, w], dtype=np.uint8)
    for i in range(0, h):
        for j in range(0, w):
            newimg[i][j] = int(0.21 * image[i][j][2] + 0.72 * image[i][j][1] + 0.07 * image[i][j][0])
    return newimg


# 8 komşuluğa bakıp 01011010 gibi sayıyı hesaplayıp 10dalık değerini döndürür
def LBPHistogram(image, i, j):
    return (128 if (image[i - 1][j - 1] >= image[i][j]) else 0) + (64 if (image[i - 1][j] >= image[i][j]) else 0) + (
        32 if (image[i - 1][j + 1] >= image[i][j]) else 0) + (16 if (image[i][j + 1] >= image[i][j]) else 0) + (
               8 if (image[i + 1][j + 1] >= image[i][j]) else 0) + (4 if (image[i + 1][j] >= image[i][j]) else 0) + (
               2 if (image[i + 1][j - 1] >= image[i][j]) else 0) + (1 if (image[i][j - 1] >= image[i][j]) else 0)


# 1-0 ve 0-1 değişimini hesaplıyor 39'a kadar, 39'dan sonra eğer değişim sayısı 2 ve 2den azsa
# lookup table'ın bir gözüne o sayıyı ekliyor değilse 58'e ekliyor.
# dönen tablo örneğin 141 sayısı verilirse 59'luk dizinin hangi gözünün artması gerektiğini belirtiyor
def createLookup():
    lookup = []
    for i in range(0, 256):
        lookup.append(0)

    empty = 0
    for i in range(0, 256):
        change = 0
        control = 128
        for j in range(0, 7):
            if (((i & control) != 0) != ((i & int(control / 2)) != 0)):
                change += 1
            control = int(control / 2)
        if (change <= 2):
            lookup[i] = empty
            empty += 1
        else:
            lookup[i] = 58
    print(lookup)
    return lookup


# Tüm piksellerin 8 komşuluğu ile lookup table kullanılarak 59luk dizi oluşturuluyor.
# Bu tabloya 58'ten sonra pixel sayısına göre normalizasyon yapılıyor.
def calculateLBP(image):
    h, w = image.shape
    tmp = np.zeros(shape=(59))
    for i in range(1, h - 1):
        for j in range(1, w - 1):
            tmp[uniform[LBPHistogram(image, i, j)]] += 1
    for i in range(0, 59):
        # tmp[i] = (tmp[i]-min(tmp))/(max(tmp)-min(tmp))
        tmp[i] = tmp[i] / (h * w)
    return tmp


# verilen 2, 59luk matrisi karşılaştırıyor
# 73ten sonra optimizasyon yapılıyor açıklaması orda var
def compareImages(target, trainI, train, min5):
    sum = 0
    for i in range(0, 59):
        sum += abs(train[trainI][i] - target[i])

        # Eğer en yakın ilk 5 dışındaki resimlere olan yakınlıklar önemseniyorsa bu kapatılmalı.
        # Bu kısım eğer resimlerin farkları toplamı en iyi 5. resimden bile kötüyse aramayı direk bırakıyor.
        if (len(min5) > 4 and min5[-1][0] < sum):
            print("Son indis değeri = ", i)
            return sum

    print("Son indis değeri = ", 59)
    return sum


# train klasörü kullanılarak eğitim yapılması, sonuçta tüm 59luk dizileri içeren => train ve count döndürüyor
def createTrain():
    train = []
    count = 0
    for i in range(0, len(traindatas)):
        image = readImage(traindatas[i])  # read image
        print(count, " = ", traindatas[i])

        lbp = calculateLBP(image)  # calculate vektor
        train.append(lbp)  # add to train data
        count += 1
    return train, count


traindatas = ["train/0001-01_1.png", "train/0001-01_2.png", "train/0001-01_3.png", "train/0001-01_4.png",
              "train/0002-01_1.png", "train/0002-01_2.png", "train/0002-01_3.png", "train/0002-01_4.png",
              "train/0006-01_1.png", "train/0006-01_2.png", "train/0006-01_3.png", "train/0006-01_4.png",
              "train/0009-01_1.png", "train/0009-01_2.png", "train/0009-01_3.png", "train/0009-01_4.png",
              "train/0010-01_1.png", "train/0010-01_2.png", "train/0010-01_3.png", "train/0010-01_4.png",
              "train/0012-01_1.png", "train/0012-01_2.png", "train/0012-01_3.png", "train/0012-01_4.png",
              "train/0022-01_1.png", "train/0022-01_2.png", "train/0022-01_3.png", "train/0022-01_4.png",
              "train/0036-01_1.png", "train/0036-01_2.png", "train/0036-01_3.png", "train/0036-01_4.png",
              "train/0039-01_1.png", "train/0039-01_2.png", "train/0039-01_3.png", "train/0039-01_4.png",
              "train/0054-01_1.png", "train/0054-01_2.png", "train/0054-01_3.png", "train/0054-01_4.png",
              "train/0061-01_1.png", "train/0061-01_2.png", "train/0061-01_3.png", "train/0061-01_4.png",
              "train/0062-01_1.png", "train/0062-01_2.png", "train/0062-01_3.png", "train/0062-01_4.png",
              "train/0065-01_1.png", "train/0065-01_2.png", "train/0065-01_3.png", "train/0065-01_4.png",
              "train/0068-01_1.png", "train/0068-01_2.png", "train/0068-01_3.png", "train/0068-01_4.png"]
# testdatas = ["test/0001-01test_1.png","test/0001-44.png","test/0002-01test_1.png","test/0002-45.png","test/0006-01test_1.png","test/0006-19.png","test/0009-01test_1.png","test/0009-41.png","test/0010-01test_1.png","test/0010-26.png","test/0012-01test_1.png","test/0012-41.png","test/0022-01test_1.png","test/0022-28.png","test/0036-01test_1.png","test/0036-37.png","test/0039-01test_1.png","test/0039-29.png","test/0054-01test_1.png","test/0054-29.png","test/0061-01test_1.png","test/0061-45.png","test/0062-01test_1.png","test/0062-28.png","test/0065-01test_1.png","test/0065-07.png","test/0068-01test_1.png","test/0068-37.png"]
testdatas = ["test2/0001-05.png", "test2/0001-44.png", "test2/0001-45.png", "test2/0001-46.png", "test2/0002-02.png",
             "test2/0002-03.png", "test2/0002-05.png", "test2/0002-32.png", "test2/0002-44.png", "test2/0002-45.png",
             "test2/0002-46.png", "test2/0006-05.png", "test2/0006-19.png", "test2/0006-37.png", "test2/0006-38.png",
             "test2/0006-39.png", "test2/0009-01.png", "test2/0009-44.png", "test2/0009-45.png", "test2/0010-06.png",
             "test2/0010-31.png", "test2/0010-32.png", "test2/0010-46.png"]

uniform = createLookup()

start = time.time()
#train, count = createTrain()
print("Process time: ", (time.time() - start))

isWhile = 1  # 1se while açık 0'sa for açık olmalı

i = 0
name = ""
# for i in range (0, len(testdatas)):
while (True):
    if isWhile:
        name = input("Hedef resmin adresi? test/?.png")
        image = readImage("test2/" + name + ".png")  # read image
    else:
        image = readImage(testdatas[i])  # read image
        print("Test edilen => ", testdatas[i])

    start = time.time()

    lbp = calculateLBP(image)  # create vektor
    result = []
    for j in range(0, count):
        compareResult = compareImages(lbp, j, train, result[:5])
        result.append([compareResult, j])
        result = sorted(result)
    print("Process time: ", (time.time() - start))

    print("\n", result[:5])

    if isWhile:
        cv2.imshow("Target", cv2.imread("test2/" + name + ".png"))
    else:
        cv2.imshow("Target", cv2.imread(testdatas[i]))

    cv2.imshow("Found 1", cv2.imread(traindatas[result[0][1]]))
    cv2.imshow("Found 2", cv2.imread(traindatas[result[1][1]]))
    cv2.imshow("Found 3", cv2.imread(traindatas[result[2][1]]))
    cv2.imshow("Found 4", cv2.imread(traindatas[result[3][1]]))
    cv2.imshow("Found 5", cv2.imread(traindatas[result[4][1]]))
    cv2.waitKey()